package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;
import java.util.stream.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.security.Principal;

@SpringBootApplication
@RestController
public class DemoApplication {
	

	@GetMapping(path = "/")
	public String helloUser() {
		return "Hello world";
	}
	
	@GetMapping(path = "/login")
	public String login(@RequestParam("code") String code) {

		return "Code "+ code;
	}


	@RequestMapping("/securedPage")
    public String securedPage(Model model, Principal principal) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println("Principal "+ authentication.getPrincipal());
		Set<String> roles = authentication.getAuthorities().stream()
			 .map(r -> r.getAuthority()).collect(Collectors.toSet());
		
		
				System.out.println(roles);
		
        return "Principal :: "+ authentication.getPrincipal() + " roles :: "+ roles.toString();
    }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
